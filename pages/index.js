import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Header from '../Layout/Header'
import Main from '../modules/Main'
import Footer from '../Layout/Footer'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Tokence</title>
        <meta name="description" content="Add tokens to your web3 wallet at one place." />
        <link rel="icon" href="/favicon.png" />
      </Head>
      <Header/>
      <Main/>
      <Footer/>
    </div>
  )
}
