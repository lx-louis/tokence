import '../styles/globals.css'
import { ChakraProvider } from '@chakra-ui/react'
import { extendTheme } from '@chakra-ui/react'
import { Global } from '@emotion/react'
import { RainbowKitProvider} from '@rainbow-me/rainbowkit';
import {WagmiConfig} from 'wagmi';
const theme = extendTheme({
  colors: {
    primary: "#1B8370",
  },
  fonts: {
    text: `'Comfortaa', cursive`,
  },
})
const Fonts = () => (
  <Global
    styles={`
      @font-face {
        font-family: 'Comfortaa';
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url('../fonts/utils/fonts/Comfortaa') format('ttf');
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
      }
      `}
  />
)

function MyApp({ Component, pageProps }) {
  return (
    <ChakraProvider theme={theme}>
      <Component {...pageProps} />
    </ChakraProvider>
  )
}

export default MyApp
