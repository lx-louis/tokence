import AppMenu from '../modules/AppMenu'
import AppMain from '../modules/AppMain'

import { Flex } from '@chakra-ui/react'

export default function app() {
    return(
        <Flex direction='row' width='100%' height="100vh">
            <AppMenu />
            <AppMain/>
        </Flex>
    )
}