import { Link } from '@chakra-ui/react'
import { Text } from '@chakra-ui/react'
import { Container } from '@chakra-ui/react'
import { Flex, Spacer } from '@chakra-ui/react'
import { theme } from '@chakra-ui/react'
import { Box } from '@chakra-ui/react'
import NextLink from "next/link"

export default function Footer() {
    return (
      <Flex direction="column" height="20vh">
        <Container margin="auto" maxWidth='auto' width='95%'>
          <Flex align='center' justify="space-between">
            <Flex align="flex-start" justify="space-around">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 20 20">
                      <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z" fill="#1B8370"/>
              </svg>
              <Text fontSize='xl' color="primary" fontFamily="text" paddingLeft="12px">HOME</Text>
            </Flex>
            <NextLink href='/app' passHref>
              <Link style={{textDecoration: 'none'}}>
                <Box fontFamily="text" borderRadius='xl' paddingTop='10px' paddingBottom='10px' 
                  margin='0' width="132px" color='#FFFFFF' textAlign='center'
                  bgGradient='linear(to-l, #21ECC7, #23A48C)'>use dApp
                </Box>
              </Link>
            </NextLink>
            
          </Flex>
        </Container>
      </Flex>
      
    )
}