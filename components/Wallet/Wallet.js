import '@rainbow-me/rainbowkit/styles.css';
import { getDefaultWallets, RainbowKitProvider, lightTheme, darkTheme} from '@rainbow-me/rainbowkit';
import { chain, configureChains, createClient, WagmiConfig } from 'wagmi';
import { publicProvider } from 'wagmi/providers/public';
import { ConnectButton } from '@rainbow-me/rainbowkit';
import { Flex } from '@chakra-ui/react';
import { Theme } from '@rainbow-me/rainbowkit';
import merge from 'lodash.merge';
const { chains, provider } = configureChains([chain.mainnet/*, chain.polygon, chain.optimism, chain.arbitrum*/],[publicProvider()]);


const myTheme2 = merge(lightTheme(), {
    colors: {
        accentColor: '#24AE95'
    },
}, Theme);
  

const { connectors } = getDefaultWallets({
    appName: 'Tokence',
    chains
});

const wagmiClient = createClient({
    autoConnect: true,
    connectors,
    provider
  })

export default function Wallet() {
    return(
        <Flex direction='row'>
            <WagmiConfig client={wagmiClient}>
                <RainbowKitProvider chains={chains} showRecentTransactions={false} theme={myTheme2}>
                        <ConnectButton/>
                </RainbowKitProvider>
            </WagmiConfig>
        </Flex>
    )
}