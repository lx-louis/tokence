import { Button, Text, Flex } from "@chakra-ui/react";
import Image from "next/image";
export default function mapToken(eth) {

    async function addToken(TokenAddress, TokenSymbol, TokenImg){
        try {
            const wasAdded = await ethereum.request({
                method: 'wallet_watchAsset',
                params: {
                type: 'ERC20',
                options: {
                    address: TokenAddress, // The address that the token is at.
                    symbol: TokenSymbol, // A ticker symbol or shorthand, up to 5 chars.
                    decimals: 18, // The number of decimals in the token
                    image: TokenImg, // A string url of the token logo
                },
                },
            });
        } 
        catch (error) {
            console.log(error);
        }
    }

    return(
        <Flex direction='column' align='center' justify='space-between' 
            border="1px solid" borderColor="primary" borderRadius="10px" padding="3">
            {/*<Image alt={`Symbol" + ${props.symbol}`} src={props.img}/>*/}
            <Text mb="2">{eth.symbol}</Text>
            <Button _hover={{backgroundColor: "#145e51"}} bg="primary" width="90%" 
                height="28px" fontSize='15px' fontWeight='light' 
                color="white" fontFamily="text" textAlign='center' 
                letterSpacing='wider' onClick={addToken(eth.address, eth.symbol, eth.img)}>
                ADD
            </Button>
        </Flex>
    )
}