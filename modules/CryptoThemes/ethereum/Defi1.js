import { Wrap, WrapItem } from '@chakra-ui/react'
import mapToken from '../../../components/Token/Token'
import eth from '../../../public/eth_defi.json'
export default function Defi1(){
    return(
        <Wrap justify="center" padding="6" spacing="6" 
            border="1px" borderColor="primary" borderRadius="2xl">
            {eth.map(mapToken)}
        </Wrap>
    )
}