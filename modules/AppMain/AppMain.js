import { Button, Container, Flex, Text } from "@chakra-ui/react";
import { Grid, GridItem } from '@chakra-ui/react'
import React, { useState } from 'react';
import { chain } from "wagmi";
import Defi1 from "../CryptoThemes/ethereum/Defi1";
import Defi2 from "../CryptoThemes/ethereum/Defi2";
import Infra from "../CryptoThemes/ethereum/Infra";
import Meta from "../CryptoThemes/ethereum/Meta";
import P2E from "../CryptoThemes/ethereum/P2E";
import Stables from "../CryptoThemes/ethereum/Stables";

export default function AppMain() {
    
    const [page, setPage] = useState("Defi")
    
    return(
        <Flex direction="column" width='80%' height='90%' marginTop='auto' marginBottom='auto'>
            <Grid templateColumns='repeat(5, 2fr)' gap={5} width="65%" margin="auto" 
                padding="6" borderRadius="2xl" boxShadow='2xl'>
                <GridItem width="100%" display="flex">
                    <Button onClick={() => setPage("Defi1")} height="48px" width="100%"
                        textAlign="center" margin="auto" fontFamily="text" borderRadius="xl" 
                        backgroundColor="white" fontWeight='light' color="black" letterSpacing="wide" 
                        _focus={{backgroundColor: "rgba(0, 0, 0, 0.24)"}} _hover={{backgroundColor: "rgba(0, 0, 0, 0.06)"}}>
                        Defi 1.0
                    </Button>
                </GridItem>
                <GridItem width="100%" display="flex" >
                    <Button onClick={() => setPage("Defi2")} height="48px" width="100%"
                        textAlign="center" margin="auto" fontFamily="text" borderRadius="xl" 
                        backgroundColor="white" fontWeight='light' color="black" letterSpacing="wide"
                        _focus={{backgroundColor: "#FEB2B2"}} _hover={{backgroundColor: "rgba(0, 0, 0, 0.06)"}}>
                        Defi 2.0
                    </Button>
                </GridItem>
                <GridItem width="100%" display="flex" >
                    <Button onClick={() => setPage("Infra")} height="48px" width="100%"
                        textAlign="center" margin="auto" fontFamily="text" borderRadius="xl" 
                        backgroundColor="white" fontWeight='light' color="black" letterSpacing="wide"
                        _focus={{backgroundColor: "#FBD38D"}} _hover={{backgroundColor: "rgba(0, 0, 0, 0.06)"}}>
                        Infra
                    </Button>
                </GridItem>
                <GridItem width="100%" display="flex">
                    <Button onClick={() => setPage("Meta")} height="48px" width="100%"
                        textAlign="center" margin="auto" fontFamily="text" borderRadius="xl" 
                        backgroundColor="white" fontWeight='light' color="black" letterSpacing="wide"
                        _focus={{backgroundColor: "#FAF089"}} _hover={{backgroundColor: "rgba(0, 0, 0, 0.06)"}}>
                        Meta
                    </Button>
                </GridItem>
                <GridItem width="100%" display="flex" >
                    <Button onClick={() => setPage("P2E")} height="48px" width="100%"
                        textAlign="center" margin="auto" fontFamily="text" borderRadius="xl" 
                        backgroundColor="white" fontWeight='light' color="black" letterSpacing="wide"
                        _focus={{backgroundColor: "#9AE6B4"}} _hover={{backgroundColor: "rgba(0, 0, 0, 0.06)"}}>
                        P2E
                    </Button>
                </GridItem>
                <GridItem width="100%" display="flex" >
                <Button onClick={() => setPage("Stables")} height="48px" width="100%"
                        textAlign="center" margin="auto" fontFamily="text" borderRadius="xl" 
                        backgroundColor="white" fontWeight='light' color="black" letterSpacing="wide"
                        _focus={{backgroundColor: "#81E6D9"}} _hover={{backgroundColor: "rgba(0, 0, 0, 0.06)"}}>
                        Stables
                    </Button>
                </GridItem>
                <GridItem width="100%" display="flex" >
                <Button onClick={() => setPage("Defi1")} height="48px" width="100%"
                        textAlign="center" margin="auto" fontFamily="text" borderRadius="xl" 
                        backgroundColor="white" fontWeight='light' color="black" letterSpacing="wide"
                        _focus={{backgroundColor: "#90cdf4"}} _hover={{backgroundColor: "rgba(0, 0, 0, 0.06)"}}>
                        Defi
                    </Button>
                </GridItem>
                <GridItem width="100%" display="flex">
                <Button onClick={() => setPage("Defi1")} height="48px" width="100%"
                        textAlign="center" margin="auto" fontFamily="text" borderRadius="xl" 
                        backgroundColor="white" fontWeight='light' color="black" letterSpacing="wide"
                        _focus={{backgroundColor: "#9DECF9"}} _hover={{backgroundColor: "rgba(0, 0, 0, 0.06)"}}>
                        Defi
                    </Button>
                </GridItem>
                <GridItem width="100%" display="flex">
                <Button onClick={() => setPage("Defi1")} height="48px" width="100%"
                        textAlign="center" margin="auto" fontFamily="text" borderRadius="xl" 
                        backgroundColor="white" fontWeight='light' color="black" letterSpacing="wide"
                        _focus={{backgroundColor: "#D6BCFA"}} _hover={{backgroundColor: "rgba(0, 0, 0, 0.06)"}}>
                        Defi
                    </Button>
                </GridItem>
                <GridItem width="100%" display="flex">
                <Button onClick={() => setPage("Defi1")} height="48px" width="100%"
                        textAlign="center" margin="auto" fontFamily="text" borderRadius="xl" 
                        backgroundColor="white" fontWeight='light' color="black" letterSpacing="wide"
                        _focus={{backgroundColor: "#FBB6CE"}} _hover={{backgroundColor: "rgba(0, 0, 0, 0.06)"}}>
                        Defi
                    </Button>
                </GridItem>
            </Grid>
            <Container margin="auto" marginInlineStart="0px"  maxWidth="50%"
                marginInlineEnd="0px" paddingInlineEnd="0px" paddingInlineStart="0px">
                {page === "Defi1" && <Defi1/>}
                {page === "Defi2" && <Defi2/>}
                {page === "Infra" && <Infra/>}
                {page === "Meta" && <Meta/>}
                {page === "P2E" && <P2E/>}
                {page === "Stables" && <Stables/>}
            </Container>
        </Flex>
    )
}