import { Box, Container, Flex, Heading, Spacer, Text } from '@chakra-ui/react'
export default function Main() {
    return (
        <Flex direction="column" height="60vh" >
        <Container margin="auto">
          <Flex direction='column' align="center" justify="center">
            <Box zIndex='3' bg='#60E2CB' p='52px' borderRadius='3xl'><Heading fontSize='5xl' color="white" fontFamily="text"
                    textAlign='center' letterSpacing='widest'>
                    TOKΞNCE
                </Heading>
                <Text fontFamily="text" textAlign='center' color="white" letterSpacing='wide'>Add tokens to your web3 wallet at one place.</Text>
            </Box>
            <Box zIndex='2' bg='#84F1DD' width='486px' height='209px' borderRadius='3xl' marginTop="-170px" marginRight='80px'></Box>
            <Box zIndex='1' bg='#B5FFF1' width='486px' height='209px' borderRadius='3xl' marginTop="-170px" marginRight='160px'></Box>
          </Flex>
        </Container>
      </Flex>
    )
}